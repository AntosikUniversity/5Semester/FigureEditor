﻿using System;
using System.Windows.Media;

namespace FigureEditor.Helpers
{
    public static class ColorHelpers
    {
        public static readonly Random Random = new Random();

        public static Color RandomColor()
        {
            return Color.FromRgb(
                (byte) Random.Next(255),
                (byte) Random.Next(255),
                (byte) Random.Next(255)
            );
        }
    }
}