﻿using System;
using System.Windows.Input;

namespace FigureEditor.ViewModels
{
    public class Command : ICommand
    {
        private readonly Action _action;
        private readonly Action<object> _actionParameterized;
        private readonly Func<bool> _func;

        public Command(Action action, Func<bool> func = null)
        {
            _action = action;
            _func = func;
        }

        public Command(Action<object> action, Func<bool> func = null)
        {
            _actionParameterized = action;
            _func = func;
        }

        public bool CanExecute(object parameter)
        {
            return _func == null ? true : _func.Invoke();
        }

        public void Execute(object parameter)
        {
            if (_func == null || _func.Invoke())
            {
                if (_action != null)
                {
                    _action.Invoke();
                }
                else if (_actionParameterized != null)
                {
                    _actionParameterized.Invoke(parameter);
                }
            }
        }

        public event EventHandler CanExecuteChanged;
    }
}