﻿using System.Windows.Media;

namespace FigureEditor.Interfaces
{
    public interface IFigure: IColorable
    {
        PointCollection Points { get; set; }
        double Top { get; set; }
        double Left { get; set; }
        double Width { get; set; }
        double Height { get; set; }
    }
}