﻿namespace FigureEditor.Interfaces
{
    public interface IFigureViewModel: IFigure
    {
        bool IsSelected { get; set; }
        string Type { get; set; }
        string Name { get; }
        uint Id { get; set; }
    }
}