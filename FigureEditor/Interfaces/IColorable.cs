﻿using System.Windows.Media;

namespace FigureEditor.Interfaces
{
    public interface IColorable
    {
        SolidColorBrush FillColor { get; set; }
        SolidColorBrush StrokeColor { get; set; }
    }
}