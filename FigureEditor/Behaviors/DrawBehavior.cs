﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using FigureEditor.Models;
using FigureEditor.ViewModels;

namespace FigureEditor.Behaviors
{
    public class DrawBehavior : Behavior<Canvas>
    {
        private bool _drawType; // for checking && _points.Clear();
        private PointCollection _points;
        private Random _random;

        protected override void OnAttached()
        {
            base.OnAttached();

            _points = new PointCollection();
            _random = new Random();

            AssociatedObject.MouseLeftButtonDown += AssociatedObject_MouseLeftButtonDown;
            AssociatedObject.MouseRightButtonDown += AssociatedObject_MouseRightButtonDown;
        }

        private void AssociatedObject_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var state = AssociatedObject.DataContext as MainViewModel;
            var drawType = state.DrawType;
            if (drawType != _drawType)
            {
                _points.Clear();
                _drawType = drawType;
            }
            if (!drawType || _points.Count < 3)
            {
                return;
            }

            var id = state.Figures.Count > 0 ? state.Figures.Max(figure => figure.Id) + 1 : 1;
            var points = _points.Clone();
            state.Figures.Add(
                new FigureViewModel(new Figure { Points = points }, id)
            );

            _points.Clear();
        }

        private void AssociatedObject_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Handled)
            {
                return;
            }

            var point = Mouse.GetPosition(AssociatedObject);
            var state = AssociatedObject.DataContext as MainViewModel;
            if (state == null) return;

            var drawType = state.DrawType;
            if (drawType != _drawType)
            {
                _points.Clear();
                _drawType = drawType;
            }
            if (drawType)
            {
                _points.Add(point);
                return;
            }

            var id = state.Figures.Count > 0 ? state.Figures.Max(figure => figure.Id) + 1 : 1;
            var width = _random.Next(50, 200);
            var height = _random.Next(50, 200);
            var left = _random.Next(0, (int) AssociatedObject.ActualWidth - width);
            var top = _random.Next(0, (int) AssociatedObject.ActualHeight - height);

            var points = new PointCollection
            {
                new Point(left, top),
                new Point(left, top + height),
                new Point(left + width, top + height),
                new Point(left + width, top)
            };
            state.Figures.Add(
                new FigureViewModel(new Figure{Points=points}, id, "Rectangle")
            );
        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseLeftButtonDown -= AssociatedObject_MouseLeftButtonDown;
            AssociatedObject.MouseRightButtonDown -= AssociatedObject_MouseRightButtonDown;

            base.OnDetaching();
        }
    }
}