﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Shapes;
using FigureEditor.ViewModels;

namespace FigureEditor.Behaviors
{
    public class ResizeBehavior : Behavior<Polygon>
    {
        private const int PointRadius = 20;
        private Canvas canvas;
        private bool isResizing;

        private Point movingPoint;
        private Point movingPointAfter;
        private int pointIndex;

        protected override void OnAttached()
        {
            base.OnAttached();

            canvas = VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(AssociatedObject)) as Canvas;

            AssociatedObject.MouseLeftButtonDown += AssociatedObject_MouseLeftButtonDown;
            AssociatedObject.MouseMove += AssociatedObject_MouseMove;
            AssociatedObject.MouseLeftButtonUp += AssociatedObject_MouseLeftButtonUp;
        }

        private void AssociatedObject_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!isResizing)
            {
                return;
            }

            var figure = AssociatedObject.DataContext as FigureViewModel;
            if (figure == null)
            {
                return;
            }

            var newFigurePoints = figure.Points.Clone();
            newFigurePoints.Insert(pointIndex, movingPointAfter);
            newFigurePoints.RemoveAt(pointIndex + 1);

            figure.Points = new PointCollection(
                newFigurePoints.Select(ToAbsolutePoint)
            );

            isResizing = false;
            AssociatedObject.ReleaseMouseCapture();
            AssociatedObject.Cursor = Cursors.Arrow;
        }

        private void AssociatedObject_MouseMove(object sender, MouseEventArgs e)
        {
            if (isResizing)
            {
                var currentPoint = e.GetPosition(canvas);
                movingPointAfter = ToRelativePoint(currentPoint);
            }
        }

        private void AssociatedObject_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Handled)
            {
                return;
            }

            var point = e.GetPosition(canvas);
            var figure = AssociatedObject.DataContext as FigureViewModel;
            if (figure == null)
            {
                return;
            }

            if (IsPointNearElement(point, figure))
            {
                isResizing = true;
                e.Handled = true;

                AssociatedObject.CaptureMouse();
                AssociatedObject.Cursor = Cursors.SizeAll;
            }
        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseLeftButtonDown -= AssociatedObject_MouseLeftButtonDown;
            AssociatedObject.MouseMove -= AssociatedObject_MouseMove;
            AssociatedObject.MouseLeftButtonUp -= AssociatedObject_MouseLeftButtonUp;

            base.OnDetaching();
        }

        /// <summary>
        ///     Shows, is our click near one of figure's points
        /// </summary>
        /// <param name="element">Figure near click</param>
        /// <param name="point">Click coordinates</param>
        /// <returns>true, if one of figure's points near our click; false, if not</returns>
        private bool IsPointNearElement(Point point, FigureViewModel element)
        {
            var relativePoint = ToRelativePoint(point);
            try
            {
                movingPoint = element.Points
                    .First(p =>
                        Math.Pow(relativePoint.X - p.X, 2) + Math.Pow(relativePoint.Y - p.Y, 2) <
                        Math.Pow(PointRadius, 2)
                    );
                pointIndex = element.Points.IndexOf(movingPoint);

                return true;
            }
            catch
            {
                return false;
            }
        }

        private Point ToRelativePoint(Point point)
        {
            var state = AssociatedObject.DataContext;
            var figure = state as FigureViewModel;

            return new Point
            {
                X = point.X - figure.Left,
                Y = point.Y - figure.Top
            };
        }

        private Point ToAbsolutePoint(Point point)
        {
            var state = AssociatedObject.DataContext;
            var figure = state as FigureViewModel;

            return new Point
            {
                X = point.X + figure.Left,
                Y = point.Y + figure.Top
            };
        }
    }
}