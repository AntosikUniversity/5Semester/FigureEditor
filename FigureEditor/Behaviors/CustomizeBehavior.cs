﻿using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Shapes;
using FigureEditor.Helpers;
using FigureEditor.ViewModels;

namespace FigureEditor.Behaviors
{
    public class CustomizeBehavior : Behavior<Shape>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.MouseEnter += AssociatedObject_MouseEnter;
        }

        private void AssociatedObject_MouseEnter(object sender, MouseEventArgs e)
        {
            var figure = AssociatedObject.DataContext as FigureViewModel;
            if (figure == null)
            {
                return;
            }
            figure.FillColor = new SolidColorBrush(ColorHelpers.RandomColor());
            figure.StrokeColor = new SolidColorBrush(ColorHelpers.RandomColor());
        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseEnter -= AssociatedObject_MouseEnter;

            base.OnDetaching();
        }
    }
}