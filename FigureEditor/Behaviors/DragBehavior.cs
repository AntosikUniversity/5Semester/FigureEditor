﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Shapes;
using FigureEditor.ViewModels;

namespace FigureEditor.Behaviors
{
    public class DragBehavior : Behavior<Polygon>
    {
        private const int PointRadius = 20;

        private Canvas canvas;
        private bool isDragging;
        private Point offsetPoint;

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.MouseLeftButtonDown += AssociatedObject_MouseLeftButtonDown;
            AssociatedObject.MouseMove += AssociatedObject_MouseMove;
            AssociatedObject.MouseLeftButtonUp += AssociatedObject_MouseLeftButtonUp;
        }

        private void AssociatedObject_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!isDragging) return;

            isDragging = false;
            AssociatedObject.ReleaseMouseCapture();
            AssociatedObject.Cursor = Cursors.Arrow;
        }

        private void AssociatedObject_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isDragging) return;

            var currentPoint = e.GetPosition(canvas);

            var figure = AssociatedObject.DataContext as FigureViewModel;
            if (figure == null) return;

            var top = currentPoint.Y - offsetPoint.Y;
            var left = currentPoint.X - offsetPoint.X;
            var maxTop = canvas.ActualHeight - figure.Height;
            var maxLeft = canvas.ActualWidth - figure.Width;

            if (top <= 0)
            {
                top = 0;
            }
            else if (top >= maxTop)
            {
                top = maxTop;
            }
            if (left <= 0)
            {
                left = 0;
            }
            else if (left >= maxLeft)
            {
                left = maxLeft;
            }

            figure.Top = top;
            figure.Left = left;
        }

        private void AssociatedObject_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Handled)
            {
                return;
            }

            if (canvas == null)
            {
                canvas = VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(AssociatedObject)) as Canvas;
            }

            offsetPoint = e.GetPosition(AssociatedObject);

            var figure = AssociatedObject.DataContext as FigureViewModel;
            if (!IsPointNearElement(offsetPoint, figure))
            {
                isDragging = true;
                AssociatedObject.CaptureMouse();
                AssociatedObject.Cursor = Cursors.ScrollAll;
                e.Handled = true;
            }
        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseLeftButtonDown -= AssociatedObject_MouseLeftButtonDown;
            AssociatedObject.MouseMove -= AssociatedObject_MouseMove;
            AssociatedObject.MouseLeftButtonUp -= AssociatedObject_MouseLeftButtonUp;

            base.OnDetaching();
        }

        /// <summary>
        ///     Shows, is our click near one of figure's points
        /// </summary>
        /// <param name="element">Figure near click</param>
        /// <param name="point">Click coordinates</param>
        /// <returns>true, if one of figure's points near our click; false, if not</returns>
        private bool IsPointNearElement(Point point, FigureViewModel element)
        {
            try
            {
                var foundPoint = element.Points
                    .First(p =>
                        Math.Pow(point.X - p.X, 2) + Math.Pow(point.Y - p.Y, 2) <
                        Math.Pow(PointRadius, 2)
                    );
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}