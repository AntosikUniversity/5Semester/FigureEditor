﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using FigureEditor.Helpers;

namespace FigureEditor.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public readonly Random Random = new Random();
        private bool _drawType; // true - polyline, false - rectangle
        private FigureViewModel _figureSelected;

        public MainViewModel()
        {
            _drawType = true;
            _figureSelected = null;

            Figures = new ObservableCollection<FigureViewModel>();

            SwitchType = new Command(() => DrawType = !DrawType);
            Customize = new Command(CustomizeFigure);
            FigureSelect = new Command(figure => { FigureSelected = figure as FigureViewModel; });
        }

        public bool DrawType
        {
            get => _drawType;
            set => Change(ref _drawType, value);
        }

        public ICommand SwitchType { get; }
        public ICommand Customize { get; }
        public ICommand FigureSelect { get; }
        public ObservableCollection<FigureViewModel> Figures { get; }

        public FigureViewModel FigureSelected
        {
            get => _figureSelected;
            set => ToggleFigureSelected(value);
        }

        public bool IsFigureSelected => _figureSelected != null;

        public void ToggleFigureSelected(FigureViewModel value)
        {
            if (_figureSelected != null) _figureSelected.IsSelected = false;
            Change(ref _figureSelected, value, new[] {"IsFigureSelected"});
            if (_figureSelected != null) _figureSelected.IsSelected = true;
        }

        private void CustomizeFigure(object param)
        {
            if (IsFigureSelected)
                switch (param as string)
                {
                    case "Fill":
                        FigureSelected.FillColor = new SolidColorBrush(ColorHelpers.RandomColor());
                        break;
                    case "Stroke":
                        FigureSelected.StrokeColor = new SolidColorBrush(ColorHelpers.RandomColor());
                        break;
                }
        }
    }
}