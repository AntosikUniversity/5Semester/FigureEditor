﻿using System.Linq;
using System.Windows.Markup;
using System.Windows.Media;
using FigureEditor.Helpers;
using FigureEditor.Interfaces;

namespace FigureEditor.ViewModels
{
    public class FigureViewModel : ViewModelBase, IFigureViewModel
    {
        private SolidColorBrush _fill;
        private double _height;
        private uint _id;
        private bool _isSelected;
        private double _left;
        private PointCollection _points;
        private SolidColorBrush _stroke;
        private double _top;
        private string _type;
        private double _width;

        public FigureViewModel(IFigure figure, uint id, string type = "Polygon")
        {
            Id = id;
            IsSelected = false;
            Type = type;
            Top = figure.Top;
            Left = figure.Left;
            Width = figure.Width;
            Height = figure.Height;
            Points = figure.Points;
            FillColor = new SolidColorBrush(ColorHelpers.RandomColor());
            StrokeColor = new SolidColorBrush(ColorHelpers.RandomColor());
        }
        
        public double Width
        {
            get => _width;
            set => Change(ref _width, value);
        }

        public double Height
        {
            get => _height;
            set => Change(ref _height, value);
        }

        public double Top
        {
            get => _top;
            set => Change(ref _top, value);
        }

        public double Left
        {
            get => _left;
            set => Change(ref _left, value);
        }

        public PointCollection Points
        {
            get => _points;
            set
            {
                /* ABSOLUTE POINTS !!! */
                var leftPoint = value.Min(p => p.X);
                var topPoint = value.Min(p => p.Y);
                var rightPoint = value.Max(p => p.X);
                var bottomPoint = value.Max(p => p.Y);
                _points = new PointCollection(value.Select(p =>
                {
                    p.X -= leftPoint;
                    p.Y -= topPoint;
                    return p;
                }));

                Width = rightPoint - leftPoint;
                Height = bottomPoint - topPoint;

                Left = leftPoint;
                Top = topPoint;
                OnPropertyChanged();
            }
        }

        [DependsOn("Type")]
        [DependsOn("Id")]
        public string Name => $"{Id}) {Type}";

        public string Type
        {
            get => _type;
            set => Change(ref _type, value);
        }

        public uint Id
        {
            get => _id;
            set => Change(ref _id, value);
        }

        public bool IsSelected
        {
            get => _isSelected;
            set => Change(ref _isSelected, value);
        }

        public SolidColorBrush FillColor
        {
            get => _fill;
            set => Change(ref _fill, value);
        }

        public SolidColorBrush StrokeColor
        {
            get => _stroke;
            set => Change(ref _stroke, value);
        }
    }
}