﻿using System.Windows.Media;
using FigureEditor.Helpers;
using FigureEditor.Interfaces;

namespace FigureEditor.Models
{
    public class Figure: IFigure, IColorable
    {
        public Figure()
        {
            FillColor = new SolidColorBrush(ColorHelpers.RandomColor());
            StrokeColor = new SolidColorBrush(ColorHelpers.RandomColor());
        }

        public PointCollection Points { get; set; }
        public double Top { get; set; }
        public double Left { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public SolidColorBrush FillColor { get; set; }
        public SolidColorBrush StrokeColor { get; set; }
    }
}