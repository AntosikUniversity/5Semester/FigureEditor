﻿using System.Windows.Controls;

namespace FigureEditor.Controls
{
    /// <summary>
    ///     Interaction logic for CanvasWithFigures.xaml
    /// </summary>
    public partial class CanvasWithFigures : UserControl
    {
        public CanvasWithFigures()
        {
            InitializeComponent();
        }
    }
}